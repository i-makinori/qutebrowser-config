
### key binds

config = config 
c = c

c.bindings.default = {}



### keybinds


## reload config

config.bind('<Ctrl-x><Ctrl-l>', 'config-source')


## utility


def bind_multiple(key, command, modes='normal'):
    if isinstance(modes, str):
        modes=[modes]
    for modes in modes :
        config.bind(key, command, mode=modes)
    


## general





# generic navigation
config.bind('H', 'back')
config.bind('F', 'forward')
config.bind('<Left>', 'back')
config.bind('<Right>', 'forward')

config.bind('O', 'set-cmd-text -s :open -t')
config.bind('o', 'set-cmd-text -s :open')
config.bind('pP', 'open -- {primary}')
config.bind('pp', 'open -- {clipboard}')


config.bind('<Ctrl-x><Ctrl-f>', 'set-cmd-text -s :open -t')
config.bind('<Ctrl-u><Ctrl-x><Ctrl-f>', 'set-cmd-text -s :open')


config.bind('h', 'scroll left')
config.bind('j', 'scroll down')
config.bind('k', 'scroll up')
config.bind('l', 'scroll right')

config.bind('gg', 'scroll-to-perc 0')
config.bind('G', 'scroll-to-perc')

config.bind('<Ctrl-v>', 'scroll-page 0 0.1')
config.bind('<Alt-v>', 'scroll-page 0 -0.1')


config.bind('<Ctrl-n>', 'scroll down')
config.bind('<Ctrl-p>', 'scroll up')
config.bind('<Ctrl-f>', 'scroll right')
config.bind('<Ctrl-b>', 'scroll left')

config.bind('T', 'tab-focus')


# yank metadat of the page
config.bind('yy', 'yank')
config.bind('yd', 'yank domain')
config.bind('ym', 'yank inline [{title}]({url})')
config.bind('yp', 'yank pretty-url')
config.bind('yt', 'yank title')

# close qutebrowser
config.bind('<Ctrl-x><Ctrl-c>', 'quit') 


# bookmark
config.bind('M', 'bookmark-add')
config.bind('b', 'set-cmd-text -s :quickmark-load')
config.bind('m', 'quickmark-save')
config.bind('B', 'set-cmd-text -s :quickmark-load -t')

# quick qute

config.bind('sb', 'open qute://bookmarks#bookmarks')
config.bind('sh', 'open qute://history')
config.bind('sq', 'open qute://bookmarks')
config.bind('ss', 'open qute://settings')

# tab managemrnt
config.bind('<Ctrl-x>0', 'tab-close')
config.bind('<Ctrl-x>1', 'tab-only')

config.bind('<Ctrl-x>bp', 'tab-prev')
config.bind('<Ctrl-x>bn', 'tab-next')
config.bind('<Up>', 'tab-prev')
config.bind('<Down>', 'tab-next')
config.bind('K', 'tab-prev')
config.bind('J', 'tab-next')


config.bind('<Ctrl-x>b1', 'tab-focus 1')
config.bind('<Ctrl-x>b2', 'tab-focus 2')
config.bind('<Ctrl-x>b3', 'tab-focus 3')
config.bind('<Ctrl-x>b4', 'tab-focus 4')
config.bind('<Ctrl-x>b5', 'tab-focus 5')
config.bind('<Ctrl-x>b6', 'tab-focus 6')
config.bind('<Ctrl-x>b7', 'tab-focus 7')
config.bind('<Ctrl-x>b8', 'tab-focus 8')
config.bind('<Ctrl-x>b9', 'tab-focus -1')

config.bind('<ctrl-x>k', 'tab-close')
config.bind('d', 'tab-close')
config.bind('D', 'tab-close -o')
config.bind('r', 'reload')
config.bind('c','yank')


# searching
config.bind('<Ctrl-s>', 'set-cmd-text /', mode='normal')
config.bind('<Ctrl-r>', 'set-cmd-text ?', mode='normal')
config.bind('<Ctrl-s>', 'search-next', mode='command')
config.bind('<Ctrl-r>', 'search-prev', mode='command')


# zooming
config.bind('+', 'zoom-in')
config.bind('-', 'zoom-out')
config.bind('0', 'zoom')


# command mode
config.bind('<Alt-x>', 'set-cmd-text :')
config.bind(':', 'set-cmd-text :')

config.bind('<Up>', 'command-history-prev', mode='command')
config.bind('<Down>', 'command-history-next', mode='command')

config.bind('<ctrl-p>', 'completion-item-focus prev', mode='command')
config.bind('<ctrl-n>', 'completion-item-focus next', mode='command')
config.bind('<alt-p>', 'command-history-prev', mode='command')
config.bind('<alt-n>', 'command-history-next', mode='command')

config.bind('<Escape>', 'leave-mode', mode='command')
config.bind('<Ctrl-g>', 'leave-mode', mode='command')
config.bind('<Return>', 'command-accept', mode='command')
config.bind('<Ctrl-m>', 'command-accept', mode='command')
config.bind('<Shift-Tab>', 'completion-item-focus prev', mode='command')
config.bind('<Ctrl-Shift-i>', 'completion-item-focus prev', mode='command')
config.bind('<Tab>', 'completion-item-focus next', mode='command')
config.bind('<Ctrl-i>', 'completion-item-focus next', mode='command')
# config.bind('<Ctrl-u>', 'completion-item-focus prev', mode='command')

config.bind('<Ctrl-h>', 'rl-backward-delete-char', mode='command')
config.bind('<Ctrl-a>', 'rl-beginning-of-line', mode='command')
config.bind('<Ctrl-e>', 'rl-end-of-line', mode='command')
config.bind('<Ctrl-y>', 'rl-yank', mode='command')


# insert mode

config.bind('i', 'enter-mode insert')
config.bind('<Ctrl-m>', 'enter-mode insert')

config.bind('<Ctrl-f>', 'fake-key <right>', mode='insert')
config.bind('<Ctrl-b>', 'fake-key <left>', mode='insert')
config.bind('<Alt-b>', 'fake-key <ctrl+left>', mode='insert')
config.bind('<Alt-f>', 'fake-key <ctrl+right>', mode='insert')
config.bind('<Ctrl-n>', 'fake-key <down>', mode='insert')
config.bind('<Ctrl-p>', 'fake-key <up>', mode='insert')
config.bind('<Ctrl-a>', 'fake-key <home>', mode='insert')
config.bind('<Ctrl-e>', 'fake-key <end>', mode='insert')
config.bind('<Ctrl-d>', 'fake-key <delete>', mode='insert')
config.bind('<Ctrl-h>', 'fake-key <Backspace>', mode='insert')
config.bind('<Ctrl-m>', 'fake-key <Return>', mode='insert')

config.bind('<Escape>', 'leave-mode', mode='insert')
config.bind('<Ctrl-g>', 'leave-mode', mode='insert')



# promt mode
config.bind('<Up>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Ctrl-p>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Down>', 'prompt-item-focus next', mode='prompt')
config.bind('<Ctrl-n>', 'prompt-item-focus next', mode='prompt')
config.bind('<Escape>', 'leave-mode', mode='prompt')
config.bind('<Ctrl-g>', 'leave-mode', mode='prompt')
config.bind('<Return>', 'prompt-accept', mode='prompt')
config.bind('<Ctrl-m>', 'prompt-accept', mode='prompt')
config.bind('<Shift-Tab>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Ctrl-Shift-i>', 'prompt-item-focus next', mode='prompt')
config.bind('<Tab>', 'prompt-item-focus next', mode='prompt')
config.bind('<Ctrl-i>', 'prompt-item-focus next', mode='prompt')
config.bind('n', 'prompt-accept no', mode='prompt')
config.bind('y', 'prompt-accept yes', mode='prompt')


# hinting
config.bind('<Ctrl-x>f', 'hint --rapid links')
config.bind('f', 'hint')
config.bind('if', 'hint images')
config.bind('cf', 'hint links yank-primary') 
config.bind('<Escape>', 'leave-mode', mode='hint')
config.bind('<Ctrl-g>', 'leave-mode', mode='hint')
config.bind('<Return>', 'follow-hint', mode='hint')
config.bind('<Ctrl-m>', 'follow-hint', mode='hint')


# yesno
config.bind('y','prompt-accept yes',mode='yesno')
config.bind('n','prompt-accept no',mode='yesno')
config.bind('<Alt-w>','prompt-yank',mode='yesno')
config.bind('<Alt-u><Alt-w>','prompt-yank --sel',mode='yesno')
config.bind('<Escape>','leave-mode',mode='yesno')
config.bind('<Ctrl-g>','leave-mode',mode='yesno')
config.bind('<Return>','prompt-accept',mode='yesno')


# caret

config.bind('$', 'move-to-end-of-line', mode='caret')
config.bind('0', 'move-to-start-of-line', mode='caret')
config.bind('<Ctrl-Space>', 'drop-selection', mode='caret')
config.bind('<Escape>', 'leave-mode', mode='caret')
config.bind('<Return>', 'yank selection', mode='caret')
config.bind('<Space>', 'toggle-selection', mode='caret')
config.bind('G', 'move-to-end-of-document', mode='caret')
config.bind('H', 'scroll left', mode='caret')
config.bind('J', 'scroll down', mode='caret')
config.bind('K', 'scroll up', mode='caret')
config.bind('L', 'scroll right', mode='caret')
config.bind('Y', 'yank selection -s', mode='caret')
config.bind('[', 'move-to-start-of-prev-block', mode='caret')
config.bind(']', 'move-to-start-of-next-block', mode='caret')
config.bind('b', 'move-to-prev-word', mode='caret')
config.bind('c', 'enter-mode normal', mode='caret')
config.bind('e', 'move-to-end-of-word', mode='caret')
config.bind('gg', 'move-to-start-of-document', mode='caret')
config.bind('h', 'move-to-prev-char', mode='caret')
config.bind('j', 'move-to-next-line', mode='caret')
config.bind('k', 'move-to-prev-line', mode='caret')
config.bind('l', 'move-to-next-char', mode='caret')
config.bind('o', 'reverse-selection', mode='caret')
config.bind('v', 'toggle-selection', mode='caret')
config.bind('w', 'move-to-next-word', mode='caret')
config.bind('y', 'yank selection', mode='caret')
config.bind('{', 'move-to-end-of-prev-block', mode='caret')
config.bind('}', 'move-to-end-of-next-block', mode='caret')




# mpv
config.bind('<Ctrl-v>', 'spawn mpv {url}')
config.bind('<Ctrl-Shift-v>', 'hint links spawn mpv {hint-url}')



